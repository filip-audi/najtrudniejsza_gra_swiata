#ifndef __CBohater_h_
#define __CBohater_h_
#include <GL/freeglut.h>
#include<math.h>
class CBohater : public CObiekt
{
	private:
		double szerokosc;
		double wysokosc;

	public:
		CBohater(void);
		CBohater(double szerokosc, double wysokosc);
		~CBohater(void);
		void Rysuj();
		int KolizjaD(CObiekt &Y);
		int KolizjaA(CObiekt &Y);
		int KolizjaW(CObiekt &Y);
		int KolizjaS(CObiekt &Y);
};
#endif __CBohater_h_