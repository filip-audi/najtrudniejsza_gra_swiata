#ifndef __CObiekt_h_
#define __CObiekt_h_
#include <GL/freeglut.h>
#include<math.h>
typedef struct
{
	float xa; //minimalna wartosc x
	float ya; //minimalna wartosc y
	float xb; //maskymalna wartosc x
	float yb;//maskymalna wartosc y
} sGranica; /*granice okreslaja lewy gorny naroznik (xa,ya) i prawy dolny (xb,yb) */
typedef struct
{
	double red;
	double green;
	double blue;
}SKolor;

class CObiekt
{
public:
	
	SKolor kolor;
	bool ukryj;
	double translacja[3];
	double rotacja[3];
	sGranica granica;//granice obiektu (obwiednia - struktura przechowuj�ca parametry obiektu, wzgl�dem �rodka, w konstruktorze ka�dego obiektu trzeba t� struktur� wype�ni�)
	float x;
	float y;//polozenie srodka masy

public:
	
	CObiekt()
	{
		translacja[0] = 0.0;
		translacja[1] = 0.0;
		translacja[2] = 0.0;
		rotacja[0] = 0.0;
		rotacja[1] = 0.0;
		rotacja[2] = 0.0;
		granica.xa=-0.1;
		granica.ya=-0.1;
		granica.xb=0.1;
		granica.yb=0.1;
				
	}
	virtual void Rysuj() = 0;
	void Ukryj();
	void Pokaz();
	void Przesun(double dX, double dY, double dZ);
	void UstawPozycje(double x, double y, double z);
	void Obroc(double rotX, double rotY, double rotZ);
	void UstawKolor(double red, double green, double blue);
	virtual int Kolizja(CObiekt &Y);
	void setGeometria(float _xa,float _ya,float _xb,float _yb);
	
};



/*
class COkrag: public CObiekt
{
private:
	double promien;
public:
	COkrag(void);
	COkrag(double Promien);
	~COkrag(void);
	void Rysuj();
};
COkrag:: COkrag()
{
	this->promien = 1;
}
COkrag:: COkrag(double Promien)
{
	this->promien = Promien;
}
COkrag:: ~COkrag()
{
}
void COkrag::Rysuj()
{
	glColor3d(this->kolor.red, this->kolor.green, this->kolor.blue);
	glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
	glBegin(GL_LINE_LOOP);
	for(float kat=0;kat<360;kat+=10)
		{
			GLfloat x,y;
			x=promien*cos(kat/180*3.14);
			y=promien*sin(kat/180*3.14);
			glVertex2f(x,y);
		}
		glEnd();
		glPopMatrix();
};



*/

#endif __CObiekt_h_