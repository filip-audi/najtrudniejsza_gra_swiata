#ifndef __CPolicjant_h_
#define __CPolicjant_h_
#include <GL/freeglut.h>
#include<math.h>
class CPolicjant: public CObiekt
{
private:
	double promien;
public:
	CPolicjant(void);
	CPolicjant(double Promien);
	~CPolicjant(void);
	void Rysuj();
	void Policjant(CPolicjant &C, int val);
};
#endif