#include "CObiekt.h"
#include "CBaza.h"

CBaza::CBaza()
{
	this->szerokosc = 0.2;
	this->wysokosc = 0.2;
}

CBaza::CBaza(double szerokosc, double wysokosc)
{
	this->szerokosc = szerokosc;
	this->wysokosc = wysokosc;
	setGeometria(-szerokosc/2,wysokosc/2,szerokosc/2,-wysokosc/2);
}

CBaza::~CBaza()
{
}

void CBaza::Rysuj()
{
		glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2]-45.0, 0.0, 0.0, 1.0);
		glColor3d(1.0, 1.0, 1.0);
		glBegin(GL_POLYGON);
		{
			glVertex3d(0.1/2, -0.01/2, 0.0);
			glVertex3d(0.1/2, 0.01/2, 0.0);
			glVertex3d(-0.1/2, 0.01/2, 0.0);
			glVertex3d(-0.1/2, -0.01/2, 0.0);
		}
		glEnd();
	glPopMatrix();
	
	glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2]+45.0, 0.0, 0.0, 1.0);
		glColor3d(1.0, 1.0, 1.0);
		glBegin(GL_POLYGON);
		{
			glVertex3d(0.1/2, -0.01/2, 0.0);
			glVertex3d(0.1/2, 0.01/2, 0.0);
			glVertex3d(-0.1/2, 0.01/2, 0.0);
			glVertex3d(-0.1/2, -0.01/2, 0.0);
		}
		glEnd();
		glPopMatrix();

		glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glColor3d(1.0, 0.0, 0.0);
		glBegin(GL_POLYGON);
		{
			glVertex3d(0.1/2, -0.1/2, 0.0);
			glVertex3d(0.1/2, 0.1/2, 0.0);
			glVertex3d(-0.1/2, 0.1/2, 0.0);
			glVertex3d(-0.1/2, -0.1/2, 0.0);
		}
		glEnd();
	glPopMatrix();

	glColor3d(this->kolor.red, this->kolor.green, this->kolor.blue);
	glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glBegin(GL_POLYGON);
		{
			glVertex3d(this->szerokosc/2, -this->wysokosc/2, 0.0);
			glVertex3d(this->szerokosc/2, this->wysokosc/2, 0.0);
			glVertex3d(-this->szerokosc/2, this->wysokosc/2, 0.0);
			glVertex3d(-this->szerokosc/2, -this->wysokosc/2, 0.0);
		}
		glEnd();
	glPopMatrix();
}
