#include "CObiekt.h"
#include "CBohater.h"

CBohater::CBohater()
{
	this->szerokosc = 0.2;
	this->wysokosc = 0.2;
}

CBohater::CBohater(double szerokosc, double wysokosc)
{
	this->szerokosc = szerokosc;
	this->wysokosc = wysokosc;
	setGeometria(-szerokosc/2,wysokosc/2,szerokosc/2,-wysokosc/2);
}

CBohater::~CBohater()
{
}
float ww=0;
void CBohater::Rysuj()
{
	glColor3d(this->kolor.red, this->kolor.green, this->kolor.blue);
	glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1]-0.02, this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glColor3d(1.0, 1.0, 1.0);
	
		
		glBegin(GL_POLYGON);
		{
	
			glVertex3d(this->szerokosc/2, -this->wysokosc/10, 0.0);
			glVertex3d(this->szerokosc/2, this->wysokosc/10, 0.0);
			glVertex3d(-this->szerokosc/2, this->wysokosc/10, 0.0);
			glVertex3d(-this->szerokosc/2, -this->wysokosc/10, 0.0);
		
		}
		glEnd();
		
	
	glPopMatrix();
	
	glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1]+0.02, this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glColor3d(1.0, 1.0, 1.0);
	
		
		glBegin(GL_POLYGON);
		{
	
			glVertex3d(this->szerokosc/2, -this->wysokosc/10, 0.0);
			glVertex3d(this->szerokosc/2, this->wysokosc/10, 0.0);
			glVertex3d(-this->szerokosc/2, this->wysokosc/10, 0.0);
			glVertex3d(-this->szerokosc/2, -this->wysokosc/10, 0.0);
		
		}
		glEnd();
		
	
	glPopMatrix();

	glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glColor3d(0.0, 0.0, 0.0);
	
		
		glBegin(GL_POLYGON);
		{
	
			glVertex3d(this->szerokosc/2, -this->wysokosc/2, 0.0);
			glVertex3d(this->szerokosc/2, this->wysokosc/2, 0.0);
			glVertex3d(-this->szerokosc/2, this->wysokosc/2, 0.0);
			glVertex3d(-this->szerokosc/2, -this->wysokosc/2, 0.0);
		
		}
		glEnd();
		
	
	glPopMatrix();

	}
	
	


int CBohater::KolizjaD(CObiekt &Y)
{
	if((Y.translacja[0]+Y.granica.xa <= this->translacja[0]+this->granica.xb+0.012) && (Y.translacja[1]+Y.granica.ya >= this->translacja[1]+this->granica.yb) && (Y.translacja[1]+Y.granica.yb <= this->translacja[1]+this->granica.ya)&& (Y.translacja[0]+Y.granica.xb >= this->translacja[0]+this->granica.xb))
		return 1;
	else return 0;
}

int CBohater::KolizjaA(CObiekt &Y)
{
	if((Y.translacja[0]+Y.granica.xb >= this->translacja[0]+this->granica.xa-0.012) && (Y.translacja[1]+Y.granica.yb <= this->translacja[1]+this->granica.ya) && (Y.translacja[1]+Y.granica.ya >= this->translacja[1]+this->granica.yb)&& (Y.translacja[0]+Y.granica.xb <= this->translacja[0]+this->granica.xb))
		return 1;
	else return 0;
}

int CBohater::KolizjaW(CObiekt &Y)
{
	if((Y.translacja[1]+Y.granica.yb <= this->translacja[1]+this->granica.ya+0.012) && (Y.translacja[0]+Y.granica.xa <= this->translacja[0]+this->granica.xb) && (Y.translacja[0]+Y.granica.xb >= this->translacja[0]+this->granica.xa)&& (Y.translacja[1]+Y.granica.ya >= this->translacja[1]+this->granica.ya))
		return 1;
	else return 0;
}

int CBohater::KolizjaS(CObiekt &Y)
{
	if((Y.translacja[1]+Y.granica.ya >= this->translacja[1]+this->granica.yb-0.012) && (Y.translacja[0]+Y.granica.xa <= this->translacja[0]+this->granica.xb) && (Y.translacja[0]+Y.granica.xb >= this->translacja[0]+this->granica.xa)&& (Y.translacja[1]+Y.granica.yb <= this->translacja[1]+this->granica.yb))
		return 1;
	else return 0;
}