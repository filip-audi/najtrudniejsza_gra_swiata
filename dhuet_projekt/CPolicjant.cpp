#include "CObiekt.h"
#include "CPolicjant.h"

CPolicjant:: CPolicjant()
{
	this->promien = 1;
}
CPolicjant:: CPolicjant(double Promien)
{
	this->promien = Promien;
	setGeometria(-promien,promien,promien,-promien);
}
CPolicjant:: ~CPolicjant()
{
}
void CPolicjant::Rysuj()
{
	glColor3d(this->kolor.red, this->kolor.green, this->kolor.blue);
	glPushMatrix();
	glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glBegin(GL_POLYGON);
		{
			glVertex3d(0.02/2, -0.03/2, 0.0);
			glVertex3d(0.02/2, 0.03/2, 0.0);
			glVertex3d(-0.02/2, 0.03/2, 0.0);
			glVertex3d(-0.02/2, -0.03/2, 0.0);
		}
		glEnd();
	glPopMatrix();
			
			
	glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glColor3d(0.0, 0.3, 0.8);
	glBegin(GL_POLYGON);
	{
		for(double i=0; i<360; i+=5)
			glVertex3d(promien*cos(i/180*3.14159), promien*sin(i/180*3.14159), 0.0);
	}
	glEnd();
			
		glPopMatrix();
		glPopMatrix();

		
	
}

void CPolicjant::Policjant(CPolicjant &C, int val)
{
	if(val%2 ==0)
	{
		this->UstawKolor(0.3,0.7,0.7);
	}
	if (val%3 == 0)
	{
		this->UstawKolor(1.0,1.0,1.0);
	}
		if (val%5 == 0)
	{
		this->UstawKolor(1.0,0.0,0.0);
	}
		this->Obroc(0.0,0.0,10.0);
}