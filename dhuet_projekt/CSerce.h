#ifndef __CSerce_h_
#define __CSerce_h_
#include <GL/freeglut.h>
#include<math.h>
class CSerce : public CObiekt
{
	private:
		double wysokosc, szerokosc;

	public:
		CSerce(void);
		CSerce(double wysokosc, double szerokosc);
		~CSerce(void);
		void Rysuj();
};
#endif