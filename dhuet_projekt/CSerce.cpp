#include "CObiekt.h"
#include "CSerce.h"

CSerce::CSerce()
{
	this->wysokosc = 0.2;
	this->szerokosc = 0.2;
}

CSerce::CSerce(double wysokosc, double szerokosc)
{
	this->szerokosc=szerokosc;
	this->wysokosc = wysokosc;
	
}

CSerce::~CSerce()
{
}

void CSerce::Rysuj()
{
	glColor3d(this->kolor.red, this->kolor.green, this->kolor.blue);

	glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glColor3d(1.0, 0.0, 0.0);
		glBegin(GL_TRIANGLES);
		{
			//glVertex3d(this->szerokosc/2, -this->wysokosc/2, 0.0);
			glVertex3f(0,-0.1,0.0f);
			glVertex3f(-0.05,-0.025,0.0f);
			glVertex3f(0.05,-0.025,0.0f);
		}
		glEnd();
		glPopMatrix();

		// drugi trojkat
		glColor3d(this->kolor.red, this->kolor.green, this->kolor.blue);

	glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glColor3d(1.0, 0.0, 0.0);
		glBegin(GL_TRIANGLES);
		{
			//glVertex3d(this->szerokosc/2, -this->wysokosc/2, 0.0);
			glVertex3f(0,0.025,0.0f);
			glVertex3f(-0.05,-0.025,0.0f);
			glVertex3f(0.05,-0.025,0.0f);
		}
		glEnd();
		glPopMatrix();

		// polkole
		glColor3d(this->kolor.red, this->kolor.green, this->kolor.blue);
	glPushMatrix();
		glTranslated(this->translacja[0]-0.024, this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2]+45.5, 0.0, 0.0, 1.0);
		glColor3d(1.0, 0.0, 0.0);
	glBegin(GL_POLYGON);
	{
		for(double i=0; i<180; i+=3)
			glVertex3d(0.035*cos(i/180*3.14159), 0.035*sin(i/180*3.14159), 0.0);
	}
	glEnd();
	glPopMatrix();

	//drugie polkole
		glColor3d(this->kolor.red, this->kolor.green, this->kolor.blue);
	glPushMatrix();
		glTranslated(this->translacja[0]+0.024, this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2]-43.5, 0.0, 0.0, 1.0);
		glColor3d(1.0, 0.0, 0.0);
	glBegin(GL_POLYGON);
	{
		for(double i=0; i<180; i+=3)
			glVertex3d(0.035*cos(i/180*3.14159), 0.035*sin(i/180*3.14159), 0.0);
	}
	glEnd();
	glPopMatrix();
}






