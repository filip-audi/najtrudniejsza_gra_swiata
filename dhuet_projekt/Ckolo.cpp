#include "CObiekt.h"
#include "Ckolo.h"

CKolo:: CKolo()
{
	this->promien = 1;
}
CKolo:: CKolo(double Promien)
{
	this->promien = Promien;
	setGeometria(-promien,promien,promien,-promien);
}
CKolo:: ~CKolo()
{
}
void CKolo::Rysuj()
{
	glColor3d(this->kolor.red, this->kolor.green, this->kolor.blue);
		glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glColor3d(0.0, 0.3, 0.8);
	glBegin(GL_POLYGON);
	{
		for(double i=0; i<360; i+=5)
			glVertex3d(promien*cos(i/180*3.14159), promien*sin(i/180*3.14159), 0.0);
	}
	glEnd();
			
		glPopMatrix();
			
}

