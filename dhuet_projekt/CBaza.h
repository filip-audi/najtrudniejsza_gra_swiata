#ifndef __CBaza_h_
#define __CBaza_h_
#include <GL/freeglut.h>
#include<math.h>
class CBaza : public CObiekt
{
	private:
		double szerokosc;
		double wysokosc;

	public:
		CBaza(void);
		CBaza(double szerokosc, double wysokosc);
		~CBaza(void);
		void Rysuj();
		
};
#endif __CBaza_h_