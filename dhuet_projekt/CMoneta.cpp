#include "CObiekt.h"
#include "CMoneta.h"

CMoneta:: CMoneta()
{
	this->promien = 1;
}
CMoneta:: CMoneta(double Promien)
{
	this->promien = Promien;
	setGeometria(-promien,promien,promien,-promien);
}
CMoneta:: ~CMoneta()
{
}
void CMoneta::Rysuj()
{
	glColor3d(this->kolor.red, this->kolor.green, this->kolor.blue);
	glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1]-0.015, this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glColor3d(0.0, 0.0, 0.0);
	
		
		glBegin(GL_POLYGON);
		{
	
			glVertex3d(0.015/2, -0.006/2, 0.0);
			glVertex3d(0.015/2, 0.006/2, 0.0);
			glVertex3d(-0.015/2, 0.006/2, 0.0);
			glVertex3d(-0.015/2, -0.006/2, 0.0);
		
		}
		glEnd();
		
	
	glPopMatrix();

		glPushMatrix();
		glTranslated(this->translacja[0]-0.008, this->translacja[1]+0.005, this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2]+45, 0.0, 0.0, 1.0);
		glColor3d(0.0, 0.0, 0.0);
	
		
		glBegin(GL_POLYGON);
		{
	
			glVertex3d(0.017/2, -0.008/2, 0.0);
			glVertex3d(0.017/2, 0.008/2, 0.0);
			glVertex3d(-0.017/2, 0.008/2, 0.0);
			glVertex3d(-0.017/2, -0.008/2, 0.0);
		
		}
		glEnd();
		
	
	glPopMatrix();

	glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glColor3d(0.0, 0.0, 0.0);
	
		
		glBegin(GL_POLYGON);
		{
	
			glVertex3d(0.008/2, -0.03/2, 0.0);
			glVertex3d(0.008/2, 0.03/2, 0.0);
			glVertex3d(-0.008/2, 0.03/2, 0.0);
			glVertex3d(-0.008/2, -0.03/2, 0.0);
		
		}
		glEnd();
		
	
	glPopMatrix();

	glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glColor3d(1.0, 1.0, 0.2);
	glBegin(GL_POLYGON);
	{
		for(double i=0; i<360; i+=5)
			glVertex3d((promien-0.01)*cos(i/180*3.14159), (promien-0.01)*sin(i/180*3.14159), 0.0);
	}
	glEnd();
			
		glPopMatrix();

		glPushMatrix();
		glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
		glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
		glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
		glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
		glColor3d(1.0, 0.65, 0.0);
	glBegin(GL_POLYGON);
	{
		for(double i=0; i<360; i+=5)
			glVertex3d(promien*cos(i/180*3.14159), promien*sin(i/180*3.14159), 0.0);
	}
	glEnd();
			
		glPopMatrix();
	

		
}