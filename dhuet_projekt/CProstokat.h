#ifndef __CProstokat_h_
#define __CProstokat_h_
#include <GL/freeglut.h>
#include<math.h>
class CProstokat : public CObiekt
{
	private:
		double szerokosc;
		double wysokosc;

	public:
		CProstokat(void);
		CProstokat(double szerokosc, double wysokosc);
		~CProstokat(void);
		void Rysuj();
		
};
#endif __CProstokat_h_