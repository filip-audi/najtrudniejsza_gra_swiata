#include "CObiekt.h"

void CObiekt::Ukryj()
{
	ukryj = true;
}

void CObiekt::Pokaz()
{
	ukryj = false;
}

void CObiekt::Przesun(double dX, double dY, double dZ)
{
	translacja[0] += dX;
	translacja[1] += dY;
	translacja[2] += dZ;
}

void CObiekt::UstawPozycje(double x, double y, double z)
{
	translacja[0] = x;
	translacja[1] = y;
	translacja[2] = z;
}

void CObiekt::Obroc(double rotX, double rotY, double rotZ)
{
	rotacja[0] += rotX;
	rotacja[1] += rotY;
	rotacja[2] += rotZ;
}

void CObiekt::UstawKolor(double red, double green, double blue)
{
	kolor.red = red;
	kolor.green = green;
	kolor.blue = blue;
}


void CObiekt::setGeometria(float _xa,float _ya,float _xb,float _yb)
{
	granica.xa=_xa;
	granica.xb=_xb;
	granica.ya=_ya;
	granica.yb=_yb;
	
}

int CObiekt::Kolizja(CObiekt &Y)
{
	if( ( (Y.translacja[0]+Y.granica.xa < this->translacja[0]+this->granica.xb) && (Y.translacja[0]+Y.granica.xb > this->translacja[0]+this->granica.xa)) && ((Y.translacja[1]+Y.granica.ya > this->translacja[1]+this->granica.yb) && (Y.translacja[1]+Y.granica.yb < this->translacja[1]+this->granica.ya))) 
		return 1;
	else return 0;
	
}
