﻿#include <GL/freeglut.h>
#include "CObiekt.h"
#include "CBohater.h"
#include "Ckolo.h"
#include "CSerce.h"
#include "CMoneta.h"
#include "CProstokat.h"
#include "CPolicjant.h"
#include "CBaza.h"
#include <stdio.h>
#include <stdlib.h>
#include <list>
#include<vector>
#include<conio.h>
#include <iostream>
#include<time.h>
#include <string>
#include<fstream>
using namespace std;


/*****************************************************
*				Zmienne globalne					 *
******************************************************/
const int glutWindowWidth = 640;
const int glutWindowHeight = 480;
float proportion = (float) glutWindowWidth / (float) glutWindowHeight;
struct SRekordy
{
	char imie[20];
	int punkty;
};
SRekordy rekordy; 


CMoneta* moneta1;
vector<CMoneta> lista_monet;
vector<CMoneta>::iterator it_monet;

CBohater* bohater1;

CProstokat* pokoj1, *pokoj2;
CProstokat* sciana_lewa;
CProstokat* sciana_prawa;
CProstokat* sciana_gora;
CProstokat* sciana_dol;
CProstokat* sciana_dol_mala;
CProstokat* sciana_gora_mala;
CProstokat* sciana_gora_lewa;
CProstokat* sciana_dol_prawa;
CProstokat* scianka1, *scianka2, *scianka3,*scianka4,*scianka5,*scianka6,*scianka7, *scianka8, *scianka9;
CBaza* baza;
CProstokat* kafelka, *start;
list<CProstokat> lista_kafelek;
list<CProstokat>::iterator it_kafelek;
list<CProstokat> lista_scian;
list<CProstokat>::iterator its;

CPolicjant* kulka1;
vector<CPolicjant> lista_kulek;
vector<CPolicjant>:: iterator it_kulek;

CSerce* serce;
list<CSerce> lista;
list<CSerce>::iterator it;


void ZapisDoPliku(char imie[20], int punkty)
{
	FILE *plik;
	int punkty1=0;
	char imie1[20];
	plik = fopen("rekordy.txt","r");
	
	if (plik!= NULL)
		{
			fscanf (plik, "imie: %s\n",&(imie1));
			fscanf(plik,"punkty: %d\n\n",&(punkty1));
			fclose(plik);
		}
	
	
	if (punkty1<= punkty || punkty1==0)
	{
		plik= fopen("rekordy.txt","w");
		if (plik!=NULL)
		{
			fprintf(plik, "imie: %s\n",imie);
			fprintf(plik,"punkty: %d\n\n",punkty);
			fclose(plik);
		}	
	cout<<"NOWY REKORD!!"<<endl;
	}
	else 
	{
		cout<<"Niestety do rekordu troche Ci zabraklo..."<<endl;
		cout<<"Rekord wynosi: "<<punkty1<<" punktow"<<endl;
	}
	cout<<"Twoj wynik to: "<<punkty<<" punktow"<<endl;

}

/* GLUT callback Handlers */
static void resize(int width, int height)
{
    const float ar = (float) width / (float) height;
	proportion = ar;

    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-ar, ar, -1.0, 1.0, 5.0, 10.0);
	gluLookAt(0, 0, 5, 0, 0, 0, 0, 1, 0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
}

static void idle(void)
{
    glutPostRedisplay();
}

static void display(void)
{
	// wyczyszenie sceny
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_LIGHTING);
	
	bohater1->Rysuj();
	
	sciana_lewa->Rysuj();
	sciana_prawa->Rysuj();
	sciana_gora->Rysuj();
	sciana_dol->Rysuj();
	sciana_dol_mala->Rysuj();
	sciana_gora_mala->Rysuj();
	sciana_gora_lewa->Rysuj();
	sciana_dol_prawa->Rysuj();
	scianka1->Rysuj();
	scianka2->Rysuj();
	scianka3->Rysuj();
	scianka4->Rysuj();
	scianka5->Rysuj();
	scianka6->Rysuj();
	scianka7->Rysuj();
	scianka8->Rysuj();
	scianka9->Rysuj();
	baza->Rysuj();
	start->Rysuj();
	
	for(it_kulek=lista_kulek.begin(); it_kulek != lista_kulek.end();it_kulek++)
	{
		it_kulek->Rysuj();
	}
	for(it_monet=lista_monet.begin(); it_monet != lista_monet.end();it_monet++)
	{
		it_monet->Rysuj();
	}
	
	for(it=lista.begin(); it != lista.end();it++)
	{
		it->Rysuj();
	}
	for(it_kafelek=lista_kafelek.begin(); it_kafelek != lista_kafelek.end();it_kafelek++)
	{
		it_kafelek->Rysuj();
	}
	pokoj1->Rysuj();
	pokoj2->Rysuj();

    glutSwapBuffers();
}

void Keyboard(unsigned char key, int x, int y)
{
	if (key == 'w')				
		{
			int kolizja = 0;
			for(its =lista_scian.begin(); its != lista_scian.end();its++)
				{
					if(bohater1->KolizjaW(*its) ==1)
						{
							kolizja = 1;
							break;
						}
				}
			if(kolizja == 0)
				{
					bohater1->Przesun(0.0,0.06,0.0);
				}
		}
	if (key == 's')
	{
		int kolizja = 0;
		for(its =lista_scian.begin(); its != lista_scian.end();its++)
			{
				if(bohater1->KolizjaS(*its) ==1)
					{
						kolizja = 1;
						break;
					}
			}
		if(kolizja == 0)
			{
				bohater1->Przesun(0.0,-0.06,0.0);
			}
	}

	if (key == 'a')				
	{
		int kolizja = 0;
		for(its =lista_scian.begin(); its != lista_scian.end();its++)
			{
				if(bohater1->KolizjaA(*its) ==1)
					{
						kolizja = 1;
						break;
					}
			}
				if(kolizja == 0)
					{
						bohater1->Przesun(-0.06,0.0,0.0);
					}
	}
	
	if (key == 'd')
	{ 
		int kolizja = 0;
		for(its =lista_scian.begin(); its != lista_scian.end();its++)
			{
				if(bohater1->KolizjaD(*its) ==1)
					{
						kolizja = 1;
						break;
					}
			}
				if(kolizja == 0)
					{
						bohater1->Przesun(0.06,0.0,0.0);
					}
	}
}

int a=1,b=1,c=1, d=1,e=1,f=1,g=1, r=1, wygrana=0;
float k=0, odl=0, k1=0;
double pozX=0.05, pozXX=-0.05, pozX3=-0.03;

void Animacja()
{
	if(wygrana == 5 && bohater1->Kolizja(*baza))
		{
			int czas = glutGet(GLUT_ELAPSED_TIME)/1000;
			rekordy.punkty =(lista.size()*200)- czas;
			glutDestroyWindow(1);
			cout<<rekordy.imie<<"! UDALO CI SIE! JESTES TERAZ WOLNY!"<<endl;
			cout<<"Twoj czas to: "<<czas<<" sekund"<<endl<<endl;
			k=0;
			a=1,b=1;
			ZapisDoPliku(rekordy.imie,rekordy.punkty);
		}
	
	for(it_monet =lista_monet.begin(); it_monet != lista_monet.end();it_monet++)
			{
				if(bohater1->Kolizja(*it_monet) ==1)
					{
						it_monet->UstawPozycje(-1.2+odl,0.75,0.0);
						odl=odl+0.1;
						wygrana+=1;
					}
			}
	
	if(lista_kulek.at(0).translacja[0]> sciana_prawa->translacja[0]-0.1 || lista_kulek.at(0).translacja[0]<sciana_lewa->translacja[0]+0.1)
		{
			a=a*-1;
		}
	lista_kulek.at(0).Przesun(pozX*a,0.0,0.0);

	if(lista_kulek.at(1).translacja[0]> sciana_prawa->translacja[0]-0.1 || lista_kulek.at(1).translacja[0]<sciana_lewa->translacja[0]+0.1)
	{
		b=b*-1;
	}
	lista_kulek.at(1).Przesun(pozXX*b,0.0,0.0);

	if(lista_kulek.at(2).translacja[0]> scianka2->translacja[0]-0.1 || lista_kulek.at(2).translacja[0]<sciana_lewa->translacja[0]+0.05)
	{
		c=c*-1;
	}
	lista_kulek.at(2).Przesun(pozX*c,0.0,0.0);

	if(lista_kulek.at(3).translacja[0]> scianka2->translacja[0]-0.07 || lista_kulek.at(3).translacja[0]<sciana_lewa->translacja[0]+0.07)
	{
		d=d*-1;
	}
	lista_kulek.at(3).Przesun(pozXX*d,0.0,0.0);

	if(lista_kulek.at(4).translacja[1]> scianka7->translacja[1]-0.07 || lista_kulek.at(4).translacja[1]<sciana_dol->translacja[1]+0.07)
	{
		e=e*-1;
	}
	if(lista_kulek.at(4).translacja[0] > sciana_prawa->translacja[0] - 0.07 || lista_kulek.at(4).translacja[0] < scianka1->translacja[0] +0.07 )
	{
		f=f*-1;
	}
	lista_kulek.at(4).Przesun((3.14/4)*0.01*f,0.01*e,0.0);
	
	lista_kulek.at(5).Przesun(0.0175*cos((k*3.14)/180), 0.0175*sin((k*3.14)/180), 0.0);  
	
	lista_kulek.at(7).Przesun(-0.0175*cos((k*3.14)/180), -0.0175*sin((k*3.14)/180), 0.0);
	lista_kulek.at(8).Przesun(-0.0175*cos((k1*3.14)/180), -0.0175*sin((k1*3.14)/180), 0.0);
	k1+=10;
	if (k1 == 360)
		{
			k1=0;
		}
	if(lista_kulek.at(9).translacja[0] < sciana_dol->translacja[0] && lista_kulek.at(9).translacja[1] <= scianka1->translacja[1] -0.08)
		{
			g=g*-1;
		}
	if(lista_kulek.at(9).translacja[1] >= scianka8->translacja[1]-0.04 && lista_kulek.at(9).translacja[0] >= sciana_lewa->translacja[0])
		{
			g=g*-1;
		}
	lista_kulek.at(9).Przesun(0.05*g,0.03*-g, 0.0); 
	k+=10;
	if (k == 360)
		{
			k=0;
		}
	for(it_kulek =lista_kulek.begin(); it_kulek != lista_kulek.end();it_kulek++)
		{
			if(bohater1->Kolizja(*it_kulek) ==1)
				{
					bohater1->UstawPozycje(-0.94,-0.54,0.0);
					lista.pop_back();
				
					if (lista.size() < 1 )
						{
							
							glutDestroyWindow(1);
							cout<<"\n"<<rekordy.imie<<"! niestety przegrales! Sprobuj jeszcze raz"<<endl<<endl;
							k=0;
							a=1,b=1;
						}
				}
		}
}
	
void timer(int val)
{
	float wartosc=0.01;
	Animacja();
	glutTimerFunc(100,timer,val+1);
	for(it_kulek=lista_kulek.begin(); it_kulek != lista_kulek.end();it_kulek++)
		{
			it_kulek->Policjant(*it_kulek,val);
		}
}


int main(int argc, char *argv[])
{
	// możliwość wyświetlania tekstu w konsoli
	printf("Witaj w NAJTRUDNIEJSZEJ GRZE SWIATA!\n\n");
	char kodznaku;
	do
		{
			cout<<"MENU:\n1. Gra jednoosobowa \n2.\nX - wyjscie\n\n";
			kodznaku = _getch();
			switch(kodznaku)
			{
				case '1':
					{ 
						system("cls");
						cout<<"Wcielasz sie w bohatera spod ciemnej gwiazdy. Niestety szczescie ostatnio cie"<<endl;
						cout<<"opuscilo, a interesy nie ida tak dobrze jak kiedys."<<endl;
						cout<<"Twoj zaufany czlowiek podczas ostatniego napadu wydaje cie policji."<<endl;
						cout<<"Trafiasz do pudla. Jednakze nie nalezysz do tych, ktorzy szybko sie poddaja."<<endl; 
						cout<<"Planujesz ucieczke. Musisz tylko zebrac piec zlotych monet, ktore pomoga ci"<<endl;
						cout<<"przekupic straznikow przy wyjsciu. Spiesz sie nie zostalo ci duzo czasu!!"<<endl;
						cout<<"Zbierz monety i ruszaj do wyjscia!"<<endl<<endl;
						cout<<"(Poruszanie sie: W - gora, S - dol, A- lewo, D-prawo )"<<endl<<endl;
						
						cout<<"PODAJ SWOJE IMIE (ZATWIERDZ ENTEREM)"<<endl<<endl;
						cin>>rekordy.imie;
						system("cls");
						lista_kulek.clear();

						//****** BOHATER *****//
						bohater1= new CBohater(0.1,0.1);
						bohater1->UstawPozycje(-0.94,-0.54,0.0);
						
						//****** MONETY *****//
						for(int i=0;i<5;++i)
						{
							moneta1 = new CMoneta(0.04);
							lista_monet.push_back(*moneta1);
						}
						lista_monet.at(0).UstawPozycje(0.9,-0.25,0.0);
						lista_monet.at(1).UstawPozycje(-0.90,0.275,0.0);
						lista_monet.at(2).UstawPozycje(-0.3,0.275,0.0);
						lista_monet.at(3).UstawPozycje(-0.6,-0.22,0.0);
						lista_monet.at(4).UstawPozycje(0.5,0.27,0.0);
						
						//****** POLICJANCI *******//
						for(int i=0;i<10;++i)
						{
							kulka1 = new CPolicjant(0.03);
							lista_kulek.push_back(*kulka1);
						}
						lista_kulek.at(0).UstawPozycje(0.0,-0.05,0.0);
						lista_kulek.at(1).UstawPozycje(0.0,0.04,0.0);
						lista_kulek.at(2).UstawPozycje(-0.45,0.385,0.0);
						lista_kulek.at(3).UstawPozycje(-0.95,0.155,0.0);
						lista_kulek.at(4).UstawPozycje(-0.40,-0.30,0.0);
						lista_kulek.at(5).UstawPozycje(0.00,0.17,0.0);
						lista_kulek.at(6).UstawPozycje(0.00,0.27,0.0);
						lista_kulek.at(7).UstawPozycje(0.00,0.37,0.0);
						lista_kulek.at(8).UstawPozycje(0.85,0.37,0.0);
						lista_kulek.at(9).UstawPozycje(-0.9,-0.18,0.0);
						
						
						//****** SCIANY PLANSZY *******//
						sciana_lewa = new CProstokat(0.01,1.02);
						sciana_lewa->UstawPozycje(-1,-0.09,0.0);
						lista_scian.push_back(*sciana_lewa);
						sciana_prawa = new CProstokat(0.01,1.02);
						sciana_prawa->UstawPozycje(1.04,0.085,0.0);
						lista_scian.push_back(*sciana_prawa);
						sciana_gora = new CProstokat(1.69,0.01);
						sciana_gora->UstawPozycje(-0.16,0.42,0.0);
						lista_scian.push_back(*sciana_gora);
						sciana_dol = new CProstokat(1.74,0.01);
						sciana_dol->UstawPozycje(0.165,-0.42,0.0);
						lista_scian.push_back(*sciana_dol);
						sciana_dol_mala = new CProstokat(0.30,0.01);
						sciana_dol_mala->UstawPozycje(-0.85,-0.6,0.0);
						lista_scian.push_back(*sciana_dol_mala);
						sciana_gora_mala = new CProstokat(0.37,0.01);
						sciana_gora_mala->UstawPozycje(0.86,0.6,0.0);
						lista_scian.push_back(*sciana_gora_mala);
						sciana_gora_lewa = new CProstokat(0.01,0.18);
						sciana_gora_lewa->UstawPozycje(0.68,0.51,0.0);
						lista_scian.push_back(*sciana_gora_lewa);
						sciana_dol_prawa = new CProstokat(0.01,0.19);
						sciana_dol_prawa->UstawPozycje(-0.7,-0.51,0.0);
						lista_scian.push_back(*sciana_dol_prawa);
						scianka1 = new CProstokat(0.01,0.3);// pierwsza pionowa od dolu patrzac
						scianka1->UstawPozycje(-0.52,-0.265,0.0);
						lista_scian.push_back(*scianka1);
						scianka2 = new CProstokat(0.01,0.29);// pierwsza pionowa dorna (od lewej)
						scianka2->UstawPozycje(-0.4,0.27,0.0);
						lista_scian.push_back(*scianka2);
						scianka3 = new CProstokat(0.01,0.29);// druga pionowa gorna
						scianka3->UstawPozycje(0.38,0.27,0.0);
						lista_scian.push_back(*scianka3);
						scianka4 = new CProstokat(0.55,0.01);// pozioma gorna, druga od lewej
						scianka4->UstawPozycje(-0.13,0.12,0.0);
						lista_scian.push_back(*scianka4);
						scianka5 = new CProstokat(0.48,0.01);// pod gorna sciana, pierwsza od lewej 
						scianka5->UstawPozycje(-0.755,0.12,0.0);
						lista_scian.push_back(*scianka5);
						scianka6 = new CProstokat(0.37,0.01);// trzecia pozioma gorna
						scianka6->UstawPozycje(0.56,0.12,0.0);
						lista_scian.push_back(*scianka6);
						scianka7 = new CProstokat(1.32,0.01);// dluga na dole, nad sciana dolna
						scianka7->UstawPozycje(0.375,-0.12,0.0);
						lista_scian.push_back(*scianka7);
						scianka8 = new CProstokat(0.25,0.01);// krotka na dole, nad mala sciana dolna
						scianka8->UstawPozycje(-0.76,-0.12,0.0);
						lista_scian.push_back(*scianka8);
						scianka9= new CProstokat(0.01,0.07);
						scianka9->UstawPozycje(-0.64,0.27,0.0);
						lista_scian.push_back(*scianka9);
						start = new CProstokat(0.30,0.19);
						start->UstawKolor(0.5,0.5,0.5);
						start->UstawPozycje(-0.85,-0.51,0.0);
						baza = new CBaza(0.37,0.18);
						baza->UstawKolor(0.976,0.878,0.294);
						baza->UstawPozycje(0.86,0.51,0.0);
						pokoj1 = new CProstokat(2.048,0.3);
						pokoj1->UstawKolor(1.0,0.8,0.6);
						pokoj1->UstawPozycje(0.025,0.27,0.0);
						pokoj2 = new CProstokat(2.048,0.3);
						pokoj2->UstawKolor(1.0,0.8,0.6);
						pokoj2->UstawPozycje(0.025,-0.27,0.0);
						float bx=0;
						for (int i=0; i<40;i++)
						{
							
							kafelka = new CProstokat(0.1025,0.12);
						
							if (i<20)
								{
									kafelka->UstawPozycje(-0.95+bx,0.06,0.0);
								}
							if (i >=20)
								{
									kafelka->UstawPozycje(-0.95+bx,0.06-0.12,0.0);
								}
							if(i%2 ==0 && i<20 || i%2 != 0 && i>=20)
								{
									kafelka->UstawKolor(0.6,0.6,0.6);
								}
							if(i%2 != 0 && i<20 || i%2 ==0 && i>=20)
								{
									kafelka->UstawKolor(0.2,0.2,0.2);
								}
							lista_kafelek.push_back(*kafelka);
							bx+=0.1024;
							if (bx >= 2.047)
							{
								bx =0;
							}
							
						}
					
						float ax = 0.1;
						for (int i=0; i<3;++i)
						{
							serce = new CSerce(0.2,0.2);
							serce->UstawPozycje(-1.3+ax,0.9,0.0);
							lista.push_back(*serce);
							ax+=0.2;
						}

						//****** GLOWNE FUNKCJE *****//
						glutInitWindowSize(glutWindowWidth, glutWindowHeight);
						glutInitWindowPosition(40,40);
						glutInit(&argc, argv);
						glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);

						glutCreateWindow("Najtrudniejsza gra swiata");
						glutTimerFunc(1000,timer,0);
						glutReshapeFunc(resize);
						glutDisplayFunc(display);
						glutIdleFunc(idle);
						glutKeyboardFunc(Keyboard);
						

						glutSetOption ( GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION ) ;

						glClearColor(1,1,1,1);

						glEnable(GL_DEPTH_TEST);
						glDepthFunc(GL_LESS);

						glEnable(GL_LIGHT0);
						glEnable(GL_NORMALIZE);
						glEnable(GL_COLOR_MATERIAL);
						glutMainLoop();
						
						
						system("pause");
						system("cls");
						break;

					}

					default :
					{
						cout<<"Podales zla liczbe..."<<endl;
						break;
						
					}

			}
		}
		while (kodznaku!='x' && kodznaku!='X');
	
	return EXIT_SUCCESS;
}